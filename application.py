import json
import random
import string

from flask import Flask, request, make_response
from flask_cors import CORS

app = Flask(__name__)
CORS(app)


@app.route('/mock/get/otp/<mobile>', methods=['GET'])
def send_otp(mobile):
	if len(mobile) != 10 or not mobile.isnumeric():
		return make_response({'success': False, 'error': 'invalid mobile no.'}, 400)
	return make_response({'success': True, 'otp': '123456'}, 200)


@app.route('/mock/otp/verify', methods=['POST'])
def verify_otp():
	"""

	:return:
	"""
	data = json.loads(request.data)
	print(data)


	return make_response({'success': True, 'otpVerification': True}, 200)


@app.route('/mock/basicInfoCapture', methods=['POST'])
def basic_info_capture():
	"""

	:return:
	"""

	data = json.loads(request.data)
	print(data)

	return make_response({'success': True}, 200)


@app.route('/mock/personalDetailCapture', methods=['POST'])
def personal_detail_capture():
	"""

	:return:
	"""

	data = json.loads(request.data)
	print(data)

	return make_response({'success': True}, 200)


@app.route('/mock/blacklisted', methods=['POST'])
def blacklist():
	"""

	:return:
	"""

	data = json.loads(request.data)
	print(data)
	return make_response({'success': True, 'blacklisted': False}, 200)


@app.route('/mock/bureauPull', methods=['POST'])
def softcell_bureau_pull():
	"""

	:return:
	"""

	data = json.loads(request.data)
	print(data)

	return make_response({'success': True, 'bureau_score': 675, 'softcell_report': 'Mock Report Generated'})


@app.route('/mock/dataParser', methods=['POST'])
def data_parser():
	"""

	:return:
	"""

	data = json.loads(request.data)
	print(data)

	loan_amount = ''.join((random.choice(string.digits) for _ in range(5)))
	return make_response({'success': True, 'loanAmount': loan_amount}, 200)


if __name__ == "__main__":
	app.run(debug=True)
