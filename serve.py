from wsgiref import simple_server

from application import app


def start_server():
    server = simple_server.make_server('0.0.0.0', 5050, app)
    server.serve_forever()


if __name__ == "__main__":
    start_server()
